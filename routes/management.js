const express = require('express');
const router = express.Router();
const sql = require('../db.js');
const dateTime = require('date-time');
const getDateFromISO = function (today) {
    let year = today.getFullYear();
    let month = today.getMonth() + 1;
    let dt = today.getDate();

    if (dt < 10) {
        dt = '0' + dt;
    }
    if (month < 10) {
        month = '0' + month;
    }

    return year + '-' + month + '-' + dt;
}


router.post("/studentscanner", (req, res) => {

    let full_search_string = "%" + req.body.search_string + "%";
    let match_search_string = req.body.search_string + "%";
    let limit_id = req.body.limit_id;

    let sqlQuery = 'SELECT CONCAT(ud.first_name," ",ud.last_name) as fullname,CONCAT(s.std_name,"-",c.section_name) as classname, ud.dob, ud.gender, ud.user_id  FROM t_user_data ud\
                    INNER JOIN t_user u ON u.user_id_fk = ud.user_id\
                    INNER JOIN t_student_class sc ON sc.student_fk = ud.user_id\
                    INNER JOIN t_class c ON c.class_id = sc.class_fk\
                    INNER JOIN t_standard s ON s.std_id = c.std_fk\
                    WHERE\
                    CONCAT(TRIM(ud.first_name), " ", TRIM(ud.last_name)) LIKE ? \
                    OR u.user_name LIKE ?\
                    OR CONCAT(TRIM(s.std_name), "-", TRIM(c.section_name)) LIKE ?\
                    LIMIT ?,?';

    sql.query(sqlQuery, [full_search_string, match_search_string, full_search_string, limit_id - 12, limit_id], (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
});


router.post("/allsessions", (req, res) => {

    let user_id = req.body.id;
    let date = getDateFromISO(new Date(dateTime()));
    let sqlQuery = 'SELECT ds.session_id,DATE_FORMAT(ds.date, "%Y-%m-%d") as date,ds.class_fk,p.period_name,s.subject_name,s.activity_flag,cs.group_name,CONCAT(ud.first_name," ",ud.last_name) as teacher_name,sh.schedule_name,ds.status_flag,sd.session_desc_name,sa.abs_flag,abs.description as abs_description \
    FROM t_daily_schedules ds LEFT JOIN t_session_descriptions sd ON sd.session_desc_id = ds.status_flag\
    INNER JOIN t_student_subjects ss ON ss.subject_fk = ds.subject_fk \
    INNER JOIN t_class c ON c.class_id = ds.class_fk \
    INNER JOIN t_periods p ON p.period_id = ds.period_fk \
    INNER JOIN t_schedules sh ON sh.schedule_id = ds.schedule_fk \
    INNER JOIN t_class_subjects cs on cs.class_fk = ds.class_fk AND cs.subject_fk = ds.subject_fk \
    INNER JOIN t_student_class sc ON sc.student_fk = ss.student_fk AND sc.class_fk = ds.class_fk \
    INNER JOIN t_user_data ud ON ud.user_id = cs.subject_teacher_fk \
    INNER JOIN t_subject s ON s.subject_id = ds.subject_fk \
    LEFT JOIN t_session_absentees sa ON sa.session_fk = ds.session_id AND sa.abs_fk = ss.student_fk \
    LEFT JOIN t_absent_description abs ON abs.id = sa.abs_flag\
    WHERE ss.student_fk = ? AND ds.date <= ? AND (ds.status_flag IN (1,8) OR ds.status_flag IS NULL) ORDER BY ds.date DESC, ds.period_fk ASC';

    sql.query(sqlQuery, [user_id, date], (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
});

router.post("/getstudentspublishedreports", (req, res) => {
    //to get user name and profile info
    let student_id = req.body.student_id;

    let sqlQuery = 'Select t1.exam_fk,t1.exam_name,t1.std_name,t1.section_name,t1.class_fk,t3.max_marks,t3.my_total,t3.failcounter from (Select e.exam_name,s.std_name,c.section_name,es.exam_schedule_id,es.exam_fk,es.class_fk,COUNT(status_flag) as TL1 from t_exam_schedules es\
      INNER JOIN t_exams e on es.exam_fk = e.exam_id\
      INNER JOIN t_class c ON c.class_id = es.class_fk\
      INNER JOIN t_standard s ON s.std_id = c.std_fk\
      INNER JOIN t_student_class sc ON sc.class_fk = c.class_id\
      INNER JOIN t_user_data u ON u.user_id = sc.student_fk\
      WHERE u.user_id=? AND es.status_flag IS NOT NULL GROUP BY es.exam_fk,es.class_fk)  t1\
      INNER JOIN\
      (Select e.exam_name,s.std_name,c.section_name,es.exam_schedule_id,es.exam_fk,es.class_fk,COUNT(status_flag = 2) as TL2 from t_exam_schedules es\
      INNER JOIN t_exams e on es.exam_fk = e.exam_id\
      INNER JOIN t_class c ON c.class_id = es.class_fk\
      INNER JOIN t_standard s ON s.std_id = c.std_fk \
      INNER JOIN t_student_class sc ON sc.class_fk = c.class_id\
      INNER JOIN t_user_data u ON u.user_id = sc.student_fk\
      WHERE u.user_id=? AND es.status_flag IS NOT NULL GROUP BY es.exam_fk,es.class_fk) t2\
      on t1.exam_schedule_id = t2.exam_schedule_id AND t1.tl1=t2.tl2 \
      INNER JOIN (SELECT q1.student_fk,q1.exam_fk,SUM(q1.group_max) as max_marks,SUM(q1.group_total) as my_total,SUM(q1.pass_flag = 0) as failcounter FROM(SELECT sqq.student_fk, sqq.fullname,sqq.report_group,sqq.group_name,sqq.exam_schedule_id,sqq.class_fk,sqq.exam_fk,sqq.mark_scored,sqq.pass_marks,SUM(sqq.mark_scored) as group_total,SUM(sqq.max_marks) as group_max,\
      SUM(sqq.mark_scored) >= sqq.pass_marks AS pass_flag FROM(SELECT DISTINCT er.student_fk,CONCAT(ud.first_name," ",ud.last_name) as fullname,\
      es.report_group,cs.group_name,es.exam_schedule_id,es.class_fk,es.exam_fk,er.mark_scored,es.pass_marks,ss.subject_fk,es.max_marks \
      FROM t_exam_schedules es \
      INNER JOIN t_exam_reports er ON er.exam_schedule_fk = es.exam_schedule_id INNER JOIN t_user_data ud ON ud.user_id = er.student_fk INNER JOIN t_class_subjects cs ON cs.group_id = es.report_group AND cs.class_fk = es.class_fk INNER JOIN t_student_subjects ss ON ss.student_fk = ud.user_id AND es.subject_fk = ss.subject_fk WHERE er.student_fk = ?) sqq GROUP BY sqq.student_fk,sqq.report_group,sqq.exam_fk)Q1 GROUP BY q1.exam_fk) t3 ON t3.exam_fk = t1.exam_fk';

    sql.query(sqlQuery, [student_id, student_id, student_id], (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
});


router.post("/getreportinfo", (req, res) => {
    //to get user name and profile info
    let exam_id = req.body.exam_id;
    let student_id = req.body.student_id;

    let sqlQuery = 'SELECT CONCAT(ud.first_name," ",ud.last_name)as fullname,ud.dob,s.std_name,c.section_name,CONCAT(ud2.first_name," ",ud2.last_name)as teachername,e.exam_name FROM t_user_data ud \
                      INNER JOIN t_student_class sc ON sc.student_fk = ud.user_id  \
                      INNER JOIN t_class c ON c.class_id = sc.class_fk \
                      INNER JOIN t_standard s ON s.std_id = c.std_fk \
                      INNER JOIN t_exams e ON e.exam_id = ? \
                      INNER JOIN t_user_data ud2 ON ud2.user_id = c.class_teacher_fk \
                      WHERE ud.user_id = ?';

    sql.query(sqlQuery, [exam_id, student_id], (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
});

router.post("/studentexamreport", (req, res) => {

    let exam_id = req.body.exam_id;
    let student_id = req.body.student_id;

    let sqlQuery = 'SELECT er.student_fk, sq.fullname,es.report_group,sq.group_name,es.subject_fk,s.subject_name,es.exam_schedule_id,er.mark_scored,er.comments,er.status_flag,es.pass_marks,es.max_marks,sq.group_total,sq.group_max,sq.pass_flag FROM t_exam_reports er INNER JOIN t_exam_schedules es ON es.exam_schedule_id = er.exam_schedule_fk INNER JOIN t_subject s ON s.subject_id = es.subject_fk INNER JOIN (SELECT sqq.student_fk, sqq.fullname,sqq.report_group,sqq.group_name,sqq.exam_schedule_id,sqq.class_fk,sqq.exam_fk,sqq.mark_scored,sqq.pass_marks,SUM(sqq.mark_scored) as group_total,SUM(sqq.max_marks) as group_max,\
    SUM(sqq.mark_scored) >= sqq.pass_marks AS pass_flag FROM(SELECT DISTINCT er.student_fk,CONCAT(ud.first_name," ",ud.last_name) as fullname,\
    es.report_group,cs.group_name,es.exam_schedule_id,es.class_fk,es.exam_fk,er.mark_scored,es.pass_marks,ss.subject_fk,es.max_marks \
    FROM t_exam_schedules es \
    INNER JOIN t_exam_reports er ON er.exam_schedule_fk = es.exam_schedule_id INNER JOIN t_user_data ud ON ud.user_id = er.student_fk INNER JOIN t_class_subjects cs ON cs.group_id = es.report_group AND cs.class_fk = es.class_fk INNER JOIN t_student_subjects ss ON ss.student_fk = ud.user_id AND es.subject_fk = ss.subject_fk WHERE es.exam_fk = ? AND er.student_fk = ?) sqq GROUP BY sqq.student_fk,sqq.report_group) sq ON sq.student_fk = er.student_fk AND sq.report_group = es.report_group WHERE es.exam_fk=?';

    sql.query(sqlQuery, [exam_id, student_id, exam_id], (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
});

router.post("/myexamavgmaxreport", (req, res) => {

    let exam_id = req.body.exam_id;
    let student_id = req.body.student_id;

    let sqlQuery = 'SELECT q1.report_group,q1.group_name,q1.highest,q2.average FROM(SELECT q.report_group,group_name,MAX(q.group_total) as highest \
        FROM (SELECT sqq.student_fk, sqq.report_group,sqq.group_name,sqq.exam_schedule_id,sqq.class_fk,sqq.exam_fk,sqq.mark_scored,sqq.pass_marks,SUM(sqq.mark_scored) as group_total,SUM(sqq.max_marks) as group_max, SUM(sqq.mark_scored) >= sqq.pass_marks AS pass_flag FROM(SELECT DISTINCT er.student_fk,es.report_group,cs.group_name,es.exam_schedule_id,es.class_fk,es.exam_fk,er.mark_scored,es.pass_marks,es.max_marks \
        FROM t_exam_schedules es\
        INNER JOIN t_exam_reports er ON er.exam_schedule_fk = es.exam_schedule_id \
        INNER JOIN t_class_subjects cs ON cs.group_id = es.report_group AND cs.class_fk = es.class_fk\
        INNER JOIN t_student_class sc ON sc.student_fk = er.student_fk WHERE es.exam_fk = ? AND es.class_fk IN ( SELECT sc.class_fk FROM t_student_class sc WHERE sc.student_fk = ?) ) sqq GROUP BY sqq.student_fk,sqq.report_group)q GROUP BY q.report_group) Q1 \
        INNER JOIN (SELECT q.report_group,group_name,ROUND(AVG(q.group_total),0) as average FROM (SELECT sqq.student_fk, sqq.report_group,sqq.group_name,sqq.exam_schedule_id,sqq.class_fk,sqq.exam_fk,sqq.mark_scored,sqq.pass_marks,SUM(sqq.mark_scored) as group_total,SUM(sqq.max_marks) as group_max, SUM(sqq.mark_scored) >= sqq.pass_marks AS pass_flag FROM(SELECT DISTINCT er.student_fk,es.report_group,cs.group_name,es.exam_schedule_id,es.class_fk,es.exam_fk,er.mark_scored,es.pass_marks,es.max_marks FROM t_exam_schedules es \
        INNER JOIN t_exam_reports er ON er.exam_schedule_fk = es.exam_schedule_id \
        INNER JOIN t_class_subjects cs ON cs.group_id = es.report_group AND cs.class_fk = es.class_fk \
        INNER JOIN t_student_class sc ON sc.student_fk = er.student_fk WHERE es.exam_fk = ? AND es.class_fk IN ( SELECT sc.class_fk FROM t_student_class sc WHERE sc.student_fk = ?) ) sqq GROUP BY sqq.student_fk,sqq.report_group)q GROUP BY q.report_group) Q2 ON q1.report_group = q2.report_group';

    sql.query(sqlQuery, [exam_id, student_id, exam_id, student_id], (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
});

router.post("/getreportclasstoppers", (req, res) => {

    let exam_id = req.body.exam_id;
    let student_id = req.body.student_id;
    let rankLimit = 6;

    let sqlQuery = 'SELECT q4.student_fk, CONCAT(ud.first_name," ",ud.last_name) as studentname,q4.total,q4.rank FROM(SELECT student_fk,failcounter,total, \
        @curRank := IF(@prevVal=total, @curRank, @studentNumber) AS rank, \
        @studentNumber := @studentNumber + 1 as studentNumber, \
        @prevVal:=total as prev \
        FROM \
        (SELECT sb1.student_fk,SUM(sb1.pass_flag = 0) as failcounter,SUM(sb1.group_total) as total \
        FROM (SELECT er.student_fk,SUM(er.mark_scored) as group_total,es.pass_marks,SUM(er.mark_scored)>=es.pass_marks as pass_flag  FROM t_exam_reports er \
        INNER JOIN t_exam_schedules es ON es.exam_schedule_id = er.exam_schedule_fk WHERE es.exam_fk=? AND es.class_fk IN ( SELECT sc.class_fk FROM t_student_class sc WHERE sc.student_fk = ?)\
        GROUP BY er.student_fk,es.report_group) sb1 GROUP BY sb1.student_fk) q,(SELECT @curRank :=0, @prevVal:=null, @studentNumber:=1) r WHERE q.failcounter = 0 ORDER BY total DESC)Q4 INNER JOIN t_user_data ud on ud.user_id = q4.student_fk WHERE q4.rank < ?  OR q4.student_fk=? ORDER by q4.rank';

    sql.query(sqlQuery, [exam_id, student_id, rankLimit, student_id,], (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
});

router.post("/studentprofileinfo", (req, res) => {
    let student_id = req.body.student_id;

    sql.query(`SELECT ud.user_id,CONCAT(ud.first_name," ",ud.last_name)as fullname,ud.dob,ud.gender,ud.joindate,ud.contact1,ud.contact2,ud.email,ud.address1,ud.address2,ud.state,ud.po_code,ud.city,u.user_name,u.password,r.role_name,CONCAT(s.std_name,"-",c.section_name) as classname,CONCAT(ud2.first_name," ",ud2.last_name)as teachername FROM t_user_data ud 
                INNER JOIN t_user u ON u.user_id_fk = ud.user_id 
                INNER JOIN t_roles r ON r.role_id = u.role_fk
                INNER JOIN t_student_class sc ON sc.student_fk = ud.user_id
                INNER JOIN t_class c ON c.class_id = sc.class_fk
                INNER JOIN t_standard s ON s.std_id = c.std_fk
                INNER JOIN t_user_data ud2 ON ud2.user_id = c.class_teacher_fk
                WHERE ud.user_id  = ?`,[student_id],(err, result) => {
            if (err) {
                res.send({
                    status: 500,
                    message: err.message,
                    content: []
                })
            }
            else {
                res.send({
                    status: 200,
                    message: "Success",
                    content: result
                })
            }
        });
})

router.post("/studentsubjectteachers", (req, res) => {
    let student_id = req.body.student_id;

    sql.query(`SELECT s.subject_name,CONCAT(ud.first_name," ",ud.last_name) as fullname  FROM t_student_subjects ss 
                INNER JOIN t_subject s ON s.subject_id = ss.subject_fk
                INNER JOIN t_student_class sc ON sc.student_fk = ss.student_fk
                INNER JOIN t_class c ON c.class_id = sc.class_fk
                INNER JOIN t_class_subjects cs ON cs.class_fk = c.class_id AND cs.subject_fk =s.subject_id
                INNER JOIN t_user_data ud ON ud.user_id = cs.subject_teacher_fk
                WHERE ss.student_fk = ?`,[student_id],(err, result) => {
            if (err) {
                res.send({
                    status: 500,
                    message: err.message,
                    content: []
                })
            }
            else {
                res.send({
                    status: 200,
                    message: "Success",
                    content: result
                })
            }
        });
})

module.exports = router;