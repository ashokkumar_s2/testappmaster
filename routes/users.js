const express = require('express');
const router = express.Router();
const sql = require('../db.js');

//routes for standards form
router.get("/info",(req,res)=>{


        let SqlQuery = 'SELECT t.first_name,t.last_name,t.dob,t.gender,t.degree,u.role_fk,r.role_name,d.dept_name,CONCAT(t.first_name," ",t.last_name) as fullName from t_user_data t LEFT JOIN t_teacher_departments td ON td.user_fk = t.user_id LEFT JOIN t_department d ON d.dept_id = td.dept_fk  INNER JOIN t_user u ON u.user_id_fk = t.user_id INNER JOIN t_roles r ON r.role_id = u.role_fk WHERE t.user_id = ?;';
        sql.query(SqlQuery,req.user.userId,(err,result)=>{
            if(err){
                res.send({
                    status: 500,
                    content:[],
                    message:err.message
                })
            }
            else{
                console.log(result);
                res.send({
                    status: 200,
                    content: result,
                    message: "Success"
                })
            }
        })
})

module.exports = router;